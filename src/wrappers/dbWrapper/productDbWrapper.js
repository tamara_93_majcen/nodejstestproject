'use strict'
let instance
const entityName = 'product'
class ProductDbWrapper {
  constructor (operations) {
    if (!operations) {
      throw new Error('Operations are undefined, product db wrapper can not be created')
    }
    this.operations = operations
  }
  async create (object) {
    let product = await this.operations.create({ entityName, object })
    return product
  }
  async readOne (readParameterName, readParameterValue) {
    let product = await this.operations.readOne({ entityName, readParameterName, readParameterValue })
    return product
  }
  async read (readParameterName = null, readParameterValue = null) {
    let products = await this.operations.read({ entityName, readParameterName, readParameterValue })
    return products
  }
  async update (updatedObject, id) {
    let updated = await this.operations.update({ entityName, updatedObject, id })
    return updated
  }
  async delete (deleteId) {
    let deleted = await this.operations.delete({ entityName, deleteId })
    return deleted
  }
}
module.exports = (operations) => {
  if (!instance) {
    instance = new ProductDbWrapper(operations)
  }
  return instance
}
