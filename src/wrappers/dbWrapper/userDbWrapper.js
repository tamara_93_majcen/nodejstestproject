'use strict'
let instance
const entityName = 'user'
class UserDbWrapper {
  constructor (operations) {
    if (!operations) {
      throw new Error('Operations are undefined, product db wrapper can not be created')
    }
    this.operations = operations
  }
  async create (object) {
    await this.operations.create({ entityName, object })
  }
  async readOne (readParameterName, readParameterValue) {
    let customer = await this.operations.readOne({ entityName, readParameterName, readParameterValue })
    return customer
  }
  async read () {
    let customer = await this.operations.read({ entityName })
    return customer
  }
  // async update (editedObject) {
  //   await this.operations.update({ entityName, editedObject })
  // }
  // async delete (deleteParameterName) {
  //   let result = await this.operations.delete({ entityName, deleteParameterName })
  //    // TODO: CHECK WHAT IS RETURNED
  // }
}
// TODO: ALL DB WRAPPERS CAN BE MERGED IN ONE, THEY ARE ALL THE SAME
module.exports = (operations) => {
  if (!instance) {
    instance = new UserDbWrapper(operations)
  }
  return instance
}
