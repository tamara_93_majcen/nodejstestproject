'use strict'
let instance
const entityName = 'customer'
class CustomerDbWrapper {
  constructor (operations) {
    if (!operations) {
      throw new Error('Operations are undefined, product db wrapper can not be created')
    }
    this.operations = operations
  }
  async create (object) {
    let created = await this.operations.create({ entityName, object })
    return created
  }
  async readOne (readParameterName, readParameterValue) {
    let customer = await this.operations.readOne({ entityName, readParameterName, readParameterValue })
    return customer
  }
  async read () {
    let customer = await this.operations.read({ entityName })
    return customer
  }
  async update (updatedObject, id) {
    let updated = await this.operations.update({ entityName, updatedObject, id })
    return updated
  }
  async delete (deleteId) {
    let deleted = await this.operations.delete({ entityName, deleteId })
    return deleted
  }
}
module.exports = (operations) => {
  if (!instance) {
    instance = new CustomerDbWrapper(operations)
  }
  return instance
}
