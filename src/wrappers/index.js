'use strict'
let instance

const customerEntityName = 'customer'
const productEntityName = 'product'
const userEntityName = 'user'

const CustomerDbWrapper = require('./dbWrapper/customerDbWrapper')
const ProductDbWrapper = require('./dbWrapper/productDbWrapper')
const UserDbWrapper = require('./dbWrapper/userDbWrapper')

class EntitiesDbWrappers {
  constructor (dbOperations, entityMap, entityHelperFunctions) {
    this.dbOperations = dbOperations
    this.entityMap = entityMap
    this.entityHelperFunctions = entityHelperFunctions
    this.wrapperMap = new Map()
    this.instantiateDbWrappers()
  }
  instantiateDbWrappers () {
    // INSTANTIATING DB CONTROLLERS
    this.wrapperMap.set(customerEntityName, CustomerDbWrapper(this.dbOperations))
    this.wrapperMap.set(productEntityName, ProductDbWrapper(this.dbOperations))
    this.wrapperMap.set(userEntityName, UserDbWrapper(this.dbOperations))
  }
}
module.exports = (dbOperations, timeSeriesDbOperations, mqttConnectionsManager) => {
  if (!instance) {
    instance = new EntitiesDbWrappers(dbOperations, timeSeriesDbOperations, mqttConnectionsManager)
  }
  return instance
}
