'use strict'
let instance
const customErrors = require('./../errorManager/errorManager')
const InvalidAuthenticationDataName = 'InvalidAuthenticationData'
const EntityNotFoundName = 'EntityNotFound'
const EntityDataNotUniqueName = 'EntityDataNotUnique'
class RoutingManager {
  constructor (router, usecaseProvider, validate, validationSchemas) {
    this.router = router
    this.usecaseProvider = usecaseProvider
    this.validate = validate
    this.validationSchemas = validationSchemas
    this.createRoutes()
  }
  createRoutes = () => {
    // customer routes
    this.router.route('/customers')
    .get(async (req, res) => {
      try {
        let usecase = this.usecaseProvider.get('ReadAllCustomersUsecase')        
        let customers = await usecase.execute()
        res.status(200).json(customers)
      } catch (e) {
          res.status(500).json(e.message)
      }
    })
    .post((req, res, next) => {
      let result = this.validate(req.body, this.validationSchemas.customerSchema)
      if (result.validationStatus) {
        next()
      } else {
        res.status(400).json('Validation failed.' + result.validationError)
      }
    },async (req, res) => {
      try {
        let usecase = this.usecaseProvider.get('CreateCustomerUsecase')
        let created = await usecase.execute(req.body)
        res.status(200).json(created)
      } catch (e) {
        if(e.name === EntityDataNotUniqueName){
          res.status(e.status).json(e.message)
        } else {
          res.status(500).json(e.message)
        }
      }
    })

  this.router.route('/customers/:id/products')
  .get(async (req, res) => {
    try {
      let usecase = this.usecaseProvider.get('ReadCustomersProductsUsecase')        
      let customer = await usecase.execute(req.params.id)
      res.status(200).json(customer)
    } catch (e) {
     if(e.name === EntityNotFoundName){
        res.status(e.status).json(e.message)
      } else {
        res.status(500).json(e.message)
      }
    }
  })

  this.router.route('/customers/:id')
    .put((req, res, next) => {
      let result = this.validate(req.body, this.validationSchemas.customerSchema)
      if (result.validationStatus) {
        next()
      } else {
        res.status(400).json('Validation failed.' + result.validationError)
      }
    }, async (req, res) => {
      try {
        let data = req.body
        data.id = req.params.id
        let usecase = this.usecaseProvider.get('UpdateCustomerUsecase')
        let updated = await usecase.execute(data)
        res.status(200).json(updated)
      } catch (e) {
        if(e.name === EntityDataNotUniqueName || e.name === EntityNotFoundName){
          res.status(e.status).json(e.message)
        } else {
          res.status(500).json(e.message)
        }
      }
    })
    .get(async (req, res) => {
      try {
        let usecase = this.usecaseProvider.get('ReadCustomerUsecase')        
        let customer = await usecase.execute(req.params.id)
        res.status(200).json(customer)
      } catch (e) {
       if(e.name === EntityNotFoundName){
          res.status(e.status).json(e.message)
        } else {
          res.status(500).json(e.message)
        }
      }
    })
    .delete(async (req, res) => {
      try {
        let usecase = this.usecaseProvider.get('DeleteCustomerUsecase')        
        let deleted = await usecase.execute(req.params.id)
        if(deleted === 0){
        res.status(200).json('Customer with this id doesn\'t exist.')
        } else {
        res.status(204)
        res.end()
        }
      } catch (e) {
          res.status(500).json(e.message)
      }
    })
    // product routes
    this.router.route('/products')
    .get(async (req, res) => {
      try {
        let usecase = this.usecaseProvider.get('ReadAllProductsUsecase')        
        let products = await usecase.execute()
        res.status(200).json(products)
      } catch (e) {
          res.status(500).json(e.message)
      }
    })
    .post((req, res, next) => {
      let result = this.validate(req.body, this.validationSchemas.productSchema)
      if (result.validationStatus) {
        next()
      } else {
        res.status(400).json('Validation failed.' + result.validationError)
      }
    }, async (req, res) => {
      try {
        let usecase = this.usecaseProvider.get('CreateProductUsecase')
        let created = await usecase.execute(req.body)
        res.status(200).json(created)
      } catch (e) {
        if(e.name === EntityDataNotUniqueName || e.name === EntityNotFoundName){
          res.status(e.status).json(e.message)
        } else {
          res.status(500).json(e.message)
        }
      }
    })

  this.router.route('/products/:id')
    .put((req, res, next) => {
      let result = this.validate(req.body, this.validationSchemas.productSchema)
      if (result.validationStatus) {
        next()
      } else {
        res.status(400).json('Validation failed.' + result.validationError)
      }
    }, async (req, res) => {
      try {
        let data = req.body
        data.id = req.params.id
        let usecase = this.usecaseProvider.get('UpdateProductUsecase')
        let updated = await usecase.execute(data)
        res.status(200).json(updated)
      } catch (e) {
        if(e.name === EntityDataNotUniqueName || e.name === EntityNotFoundName){
          res.status(e.status).json(e.message)
        } else {
          res.status(500).json(e.message)
        }
      }
    })
    .get(async (req, res) => {
      try {
        let usecase = this.usecaseProvider.get('ReadProductUsecase')        
        let product = await usecase.execute(req.params.id)
        res.status(200).json(product)
      } catch (e) {
        res.status(500).json(e.message)
      }
    })
    .delete(async (req, res) => {
      try {
        let usecase = this.usecaseProvider.get('DeleteProductUsecase')        
        let deleted = await usecase.execute(req.params.id)
        if(deleted === 0){
        res.status(200).json('Product with this id doesn\'t exist')
        } else {
        res.status(204)
        res.end()
        }
      } catch (e) {
          res.status(500).json(e.message)
      }
    })
  }
}

module.exports = (router, entitiesDbWrappers, validate, validationSchemas) => {
  if (!instance) {
    instance = new RoutingManager(router, entitiesDbWrappers,  validate, validationSchemas)
  }
  return instance
}
