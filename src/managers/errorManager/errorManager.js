'use strict'
class InvalidAuthenticationData {
  constructor (message) {
    this.name = 'InvalidAuthenticationData'
    this.message = message
    this.status = 400
  }
}
class EntityDataNotUnique {
  constructor (message) {
    this.name = 'EntityDataNotUnique'
    this.message = message
    this.status = 400
  }
}
class EntityNotFound {
  constructor (message) {
    this.name = 'EntityNotFound'
    this.message = message
    this.status = 404
  }
}

module.exports = {
  InvalidAuthenticationData: InvalidAuthenticationData,
  EntityDataNotUnique: EntityDataNotUnique,
  EntityNotFound: EntityNotFound
}
