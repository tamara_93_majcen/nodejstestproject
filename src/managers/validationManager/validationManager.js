'use strict'
let instance
class ValidationManager {
  constructor (validator) {
    this.validator = validator.validate
  }
  validate = (objectToValidate, schema) => {
    return this.validator(objectToValidate, schema)
  }
}
module.exports = (validator) => {
  if (!instance) {
    instance = new ValidationManager(validator)
  }
  return instance
}
