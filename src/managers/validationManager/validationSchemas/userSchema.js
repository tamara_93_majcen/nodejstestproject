'use strict'
const userSchema = {
  username: { type: 'string', min: 4, max: 45 },
  password: { type: 'string', min: 4, max: 45 }
}
module.exports = userSchema
