
'use strict'
module.exports = {
  first_name: { type: 'string', min: 3, max: 25 },
  last_name: { type: 'string', min: 3, max: 25 },
  email: { type: 'email', max: 100 }
}
