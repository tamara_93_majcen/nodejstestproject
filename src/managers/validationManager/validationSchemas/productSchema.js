'use strict'
module.exports = {
  customer_id: { type: 'number', positive: true, integer: true },
  name: { type: 'string', min: 3, max: 100 },
  quantity: { type: 'number', positive: true, integer: true } // TODO: CHECK FOR 0 VALUE
}
