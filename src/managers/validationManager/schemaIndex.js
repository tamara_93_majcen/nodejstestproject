'use strict'
const productSchema = require('./validationSchemas/productSchema')
const customerSchema = require('./validationSchemas/customerSchema')
const userSchema = require('./validationSchemas/userSchema')

module.exports = {
  productSchema: productSchema,
  customerSchema: customerSchema,
  userSchema: userSchema
}
