'use strict'
const customErrors = require('./../../../../managers/errorManager/errorManager')
let instance
const readParameterCustomerId = 'customer_id'
const readParameterId = 'id'
const entityName = 'customer'
class ReadCustomersProductsUsecase {
  constructor (customerFactory, dbWrapperMap, helperFunctions) {
    this.customerFactory = customerFactory
    this.dbWrapperMap = dbWrapperMap
    this.helperFunctions = helperFunctions
  }

  async execute (customerId) {
    let customerDbWrapper = this.dbWrapperMap.get(entityName)
    let customerResult = await customerDbWrapper.readOne(readParameterId, customerId)
    if (customerResult) {
      let productDbWrapper = this.dbWrapperMap.get('product')
      let productsResult = await productDbWrapper.read(readParameterCustomerId, customerId)
      return productsResult
    } else {
      throw new customErrors.EntityNotFound('Customer with this id doesn\'t exist')
    }
  }
}

module.exports = (customerFactory, dbWrapperMap, helperFunctions) => {
  if (!instance) {
    instance = new ReadCustomersProductsUsecase(customerFactory, dbWrapperMap, helperFunctions)
  }
  return instance
}
