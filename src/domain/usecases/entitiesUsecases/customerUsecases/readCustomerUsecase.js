'use strict'
const customErrors = require('./../../../../managers/errorManager/errorManager')
let instance
const readParameterId = 'id'
const entityName = 'customer'
class ReadCustomerUsecase {
  constructor (customerFactory, dbWrapperMap, helperFunctions) {
    this.customerFactory = customerFactory
    this.dbWrapperMap = dbWrapperMap
    this.helperFunctions = helperFunctions
  }

  async execute (customerId) {
    let customerDbWrapper = this.dbWrapperMap.get(entityName)
    let customerResult = await customerDbWrapper.readOne(readParameterId, customerId)
    if (customerResult) {
      return customerResult
    } else {
      throw new customErrors.EntityNotFound('Customer with this id doesn\'t exist')
    }
  }
}

module.exports = (customerFactory, dbWrapperMap, helperFunctions) => {
  if (!instance) {
    instance = new ReadCustomerUsecase(customerFactory, dbWrapperMap, helperFunctions)
  }
  return instance
}
