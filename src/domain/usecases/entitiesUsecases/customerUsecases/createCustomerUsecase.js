'use strict'
const customErrors = require('./../../../../managers/errorManager/errorManager')
const entityName = 'customer'
const searchParameterEmail = 'email'
let instance

class CreateCustomerUsecase {
  constructor (customerFactory, dbWrapperMap, helperFunctions) {
    this.customerFactory = customerFactory
    this.dbWrapperMap = dbWrapperMap
    this.helperFunctions = helperFunctions
  }

  async execute (customer) {
    let customerDbWrapper = this.dbWrapperMap.get(entityName)
    let customerEntity = this.customerFactory.makeCustomer(customer)
    let customerByEmail = await this.helperFunctions.entityExists(entityName, this.dbWrapperMap, searchParameterEmail, customerEntity.getCustomerEmail())
    if (customerByEmail) {
      throw new customErrors.EntityDataNotUnique('Customer with this email already exists. Please enter unique email.')
    } else {
      let created = await customerDbWrapper.create({
        first_name: customerEntity.getCustomerFirstName(),
        last_name: customerEntity.getCustomerLastName(),
        email: customerEntity.getCustomerEmail()
      })
      return created
    }
  }
}

module.exports = (customerFactory, dbWrapperMap, helperFunctions) => {
  if (!instance) {
    instance = new CreateCustomerUsecase(customerFactory, dbWrapperMap, helperFunctions)
  }
  return instance
}
