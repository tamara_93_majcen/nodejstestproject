'use strict'
const customErrors = require('./../../../../managers/errorManager/errorManager')
let instance
const entityName = 'customer'
const readParameterId = 'id'
class DeleteCustomerUsecase {
  constructor (customerFactory, dbWrapperMap, helperFunctions) {
    this.customerFactory = customerFactory
    this.dbWrapperMap = dbWrapperMap
    this.helperFunctions = helperFunctions
  }

  async execute (deleteId) {
    let customerDbWrapper = this.dbWrapperMap.get(entityName)
    let deleted = await customerDbWrapper.delete(deleteId)
    return deleted
  }
}

module.exports = (customerFactory, dbWrapperMap, helperFunctions) => {
  if (!instance) {
    instance = new DeleteCustomerUsecase(customerFactory, dbWrapperMap, helperFunctions)
  }
  return instance
}
