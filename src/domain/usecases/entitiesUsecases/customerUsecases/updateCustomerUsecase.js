'use strict'
const customErrors = require('./../../../../managers/errorManager/errorManager')
let instance
const entityName = 'customer'
const searchParameterId = 'id'
const searchParameterEmail = 'email'
class UpdateCustomerUsecase {
  constructor (customerFactory, dbWrapperMap, helperFunctions) {
    this.customerFactory = customerFactory
    this.dbWrapperMap = dbWrapperMap
    this.helperFunctions = helperFunctions
  }

  async execute (customer) {
    let customerDbWrapper = this.dbWrapperMap.get(entityName)
    let customerEntity = this.customerFactory.makeCustomer(customer)
    let customerById = await this.helperFunctions.entityExists(entityName, this.dbWrapperMap, searchParameterId, customerEntity.getCustomerId())
    if (customerById) {
      let customerByEmail = await this.helperFunctions.entityExists(entityName, this.dbWrapperMap, searchParameterEmail, customerEntity.getCustomerEmail())
      if (customerByEmail && customerByEmail.id !== customerById.id) {
        throw new customErrors.EntityDataNotUnique('Customer with this email already exists. Please insert unique email.')
      } else {
        let updated = await customerDbWrapper.update({
          first_name: customerEntity.getCustomerFirstName(),
          last_name: customerEntity.getCustomerLastName(),
          email: customerEntity.getCustomerEmail()
        },
        customerEntity.getCustomerId()
        )
        if (updated === 1) {
          let updatedCustomer = await customerDbWrapper.readOne(searchParameterId, customerEntity.getCustomerId())
          return updatedCustomer
        } else {
          return null
        }
      }
    } else {
      throw new customErrors.EntityNotFound('Customer you are trying to update doesn\'t exist.')
    }
  }
}

module.exports = (customerFactory, dbWrapperMap, helperFunctions) => {
  if (!instance) {
    instance = new UpdateCustomerUsecase(customerFactory, dbWrapperMap, helperFunctions)
  }
  return instance
}
