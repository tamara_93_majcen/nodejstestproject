'use strict'
let instance
const entityName = 'customer'
class ReadAllCustomersUsecase {
  constructor (customerFactory, dbWrapperMap, helperFunctions) {
    this.customerFactory = customerFactory
    this.dbWrapperMap = dbWrapperMap
    this.helperFunctions = helperFunctions
  }

  async execute () {
    let customerDbWrapper = this.dbWrapperMap.get(entityName)
    let customerResult = await customerDbWrapper.read()
    return customerResult
  }
}

module.exports = (customerFactory, dbWrapperMap, helperFunctions) => {
  if (!instance) {
    instance = new ReadAllCustomersUsecase(customerFactory, dbWrapperMap, helperFunctions)
  }
  return instance
}
