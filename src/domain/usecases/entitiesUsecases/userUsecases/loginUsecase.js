'use strict'
let instance
const customErrors = require('./../../../../managers/errorManager/errorManager')
const entityName = 'user'
const searchParameterUsername = 'username'
class LoginUsecase {
  constructor (userFactory, dbWrapperMap, helperFunctions) {
    this.userFactory = userFactory
    this.dbWrapperMap = dbWrapperMap
    this.helperFunctions = helperFunctions
  }

  async execute (user) {
    let userEntity = this.userFactory.makeUser(user)
    let hashedPassword = this.helperFunctions.hashPassword(userEntity.getPassword())
    let userExists = await this.helperFunctions.entityExists(entityName, this.dbWrapperMap, searchParameterUsername, userEntity.getUsername())
    if (userExists) {
      if (userExists.password === hashedPassword) {
        return userExists
      } else {
        throw new customErrors.InvalidAuthenticationData('Wrong password.')
      }
    }
    return userExists
  }
}

module.exports = (customerFactory, dbWrapperMap, helperFunctions) => {
  if (!instance) {
    instance = new LoginUsecase(customerFactory, dbWrapperMap, helperFunctions)
  }
  return instance
}
