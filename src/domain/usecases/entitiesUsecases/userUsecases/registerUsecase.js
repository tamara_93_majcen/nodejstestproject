'use strict'
const customErrors = require('./../../../../managers/errorManager/errorManager')
let instance
const entityName = 'user'
const searchParameterUsername = 'username'
class RegisterUsecase {
  constructor (userFactory, dbWrapperMap, helperFunctions) {
    this.userFactory = userFactory
    this.dbWrapperMap = dbWrapperMap
    this.helperFunctions = helperFunctions
  }

  async execute (user) {
    let userDbWrapper = this.dbWrapperMap.get(entityName)
    let userEntity = this.userFactory.makeUser(user)
    // checking if user with this username already exists
    let userExists = await this.helperFunctions.entityExists(entityName, this.dbWrapperMap, searchParameterUsername, userEntity.getUsername())
    if (userExists) {
      throw new customErrors.EntityDataNotUnique('User with this username already exists.')
    } else {
      let hashedPassword = this.helperFunctions.hashPassword(userEntity.getPassword())
      userEntity.setHashedPassword(hashedPassword)
      await userDbWrapper.create({
        username: userEntity.getUsername(),
        password: userEntity.getPassword()
      })
    }
  }
}

module.exports = (customerFactory, dbWrapperMap, helperFunctions) => {
  if (!instance) {
    instance = new RegisterUsecase(customerFactory, dbWrapperMap, helperFunctions)
  }
  return instance
}
