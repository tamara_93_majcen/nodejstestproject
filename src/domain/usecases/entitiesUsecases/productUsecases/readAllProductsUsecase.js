'use strict'
let instance
const entityName = 'product'
class ReadAllProductsUsecase {
  constructor (productFactory, dbWrapperMap, helperFunctions) {
    this.productFactory = productFactory
    this.dbWrapperMap = dbWrapperMap
    this.helperFunctions = helperFunctions
  }

  async execute () {
    let productDbWrapper = this.dbWrapperMap.get(entityName)
    let productsResult = await productDbWrapper.read()
    return productsResult
  }
}

module.exports = (productFactory, dbWrapperMap, helperFunctions) => {
  if (!instance) {
    instance = new ReadAllProductsUsecase(productFactory, dbWrapperMap, helperFunctions)
  }
  return instance
}
