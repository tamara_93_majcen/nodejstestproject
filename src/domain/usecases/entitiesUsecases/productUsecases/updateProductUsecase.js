'use strict'
const customErrors = require('./../../../../managers/errorManager/errorManager')
let instance
const productEntityName = 'product'
const customerEntityName = 'customer'
const searchParameterId = 'id'
const searchParameterName = 'name'

class UpdateProductUsecase {
  constructor (productFactory, dbWrapperMap, helperFunctions) {
    this.productFactory = productFactory
    this.dbWrapperMap = dbWrapperMap
    this.helperFunctions = helperFunctions
  }

  async execute (product) {
    let productDbWrapper = this.dbWrapperMap.get(productEntityName)
    let productEntity = this.productFactory.makeProduct(product)
    let productExists = await this.helperFunctions.entityExists(productEntityName, this.dbWrapperMap, searchParameterId, productEntity.getProductId())
    if (productExists) {
      let productWithSameName = await this.helperFunctions.entityExists(productEntityName, this.dbWrapperMap, searchParameterName, productEntity.getProductName())
      let productId = parseInt(productEntity.getProductId())
      if (productWithSameName && productWithSameName.id !== productId) {
        throw new customErrors.EntityDataNotUnique('Product with this name already exists.')
      } else {
        let customerExists = await this.helperFunctions.entityExists(customerEntityName, this.dbWrapperMap, searchParameterId, productEntity.getProductCustomerId())
        if (customerExists) {
          let updated = await productDbWrapper.update({
            name: productEntity.getProductName(),
            quantity: productEntity.getProductQuantity(),
            customer_id: productEntity.getProductCustomerId()
          },
          productEntity.getProductId()
          )
          if (updated === 1) {
            let updatedProduct = await productDbWrapper.readOne(searchParameterId, productEntity.getProductId())
            return updatedProduct
          } else {
            return null
          }
        } else {
          throw new customErrors.EntityNotFound('Customer with this id doesn\'t exist.')
        }
      }
    } else {
      throw new customErrors.EntityNotFound('Product you\'re trying to update doesn\'t exist.')
    }
  }
}

module.exports = (productFactory, dbWrapperMap, helperFunctions) => {
  if (!instance) {
    instance = new UpdateProductUsecase(productFactory, dbWrapperMap, helperFunctions)
  }
  return instance
}
