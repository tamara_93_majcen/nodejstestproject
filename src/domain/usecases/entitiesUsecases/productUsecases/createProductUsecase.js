'use strict'
const customErrors = require('./../../../../managers/errorManager/errorManager')
let instance
const productEntityName = 'product'
const customerEntityName = 'customer'
const searchParameterId = 'id'
const searchParameterName = 'name'
class CreateProductUsecase {
  constructor (productFactory, dbWrapperMap, helperFunctions) {
    this.productFactory = productFactory
    this.dbWrapperMap = dbWrapperMap
    this.helperFunctions = helperFunctions
  }

  async execute (product) {
    let productDbWrapper = this.dbWrapperMap.get(productEntityName)
    let productEntity = this.productFactory.makeProduct(product)
    let customerExists = await this.helperFunctions.entityExists(customerEntityName, this.dbWrapperMap, searchParameterId, productEntity.getProductCustomerId())
    if (customerExists) {
      let productWithSameNameExists = await this.helperFunctions.entityExists(productEntityName, this.dbWrapperMap, searchParameterName, productEntity.getProductName())
      if (productWithSameNameExists) {
        throw new customErrors.EntityDataNotUnique('Product with this name already exists')
      } else {
        let product = await productDbWrapper.create({
          name: productEntity.getProductName(),
          quantity: productEntity.getProductQuantity(),
          customer_id: productEntity.getProductCustomerId()
        })
        return product
      }
    } else {
      throw new customErrors.EntityNotFound('Customer with this id doesn\'t exist')
    }
  }
}

module.exports = (productFactory, dbWrapperMap, helperFunctions) => {
  if (!instance) {
    instance = new CreateProductUsecase(productFactory, dbWrapperMap, helperFunctions)
  }
  return instance
}
