'use strict'
const customErrors = require('./../../../../managers/errorManager/errorManager')
let instance
const productEntityName = 'product'
const readParameterId = 'id'
class DeleteProductUsecase {
  constructor (productFactory, dbWrapperMap, helperFunctions) {
    this.productFactory = productFactory
    this.dbWrapperMap = dbWrapperMap
    this.helperFunctions = helperFunctions
  }

  async execute (deleteId) {
    let productDbWrapper = this.dbWrapperMap.get(productEntityName)
    let deleted = await productDbWrapper.delete(deleteId)
    return deleted
  }
}

module.exports = (productFactory, dbWrapperMap, helperFunctions) => {
  if (!instance) {
    instance = new DeleteProductUsecase(productFactory, dbWrapperMap, helperFunctions)
  }
  return instance
}
