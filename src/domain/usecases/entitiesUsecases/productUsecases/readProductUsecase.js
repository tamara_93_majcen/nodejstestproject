'use strict'
const customErrors = require('./../../../../managers/errorManager/errorManager')
let instance
const readParameterId = 'id'
class CreateProductUsecase {
  constructor (productFactory, dbWrapperMap, helperFunctions) {
    this.productFactory = productFactory
    this.dbWrapperMap = dbWrapperMap
    this.helperFunctions = helperFunctions
  }

  async execute (productId) {
    let productDbWrapper = this.dbWrapperMap.get('product')
    let productResult = await productDbWrapper.readOne(readParameterId, productId)
    if (productResult) {
      return productResult
    } else {
      throw new customErrors.EntityNotFound('Product with this id doesn\'t exist')
    }
  }
}

module.exports = (productFactory, dbWrapperMap, helperFunctions) => {
  if (!instance) {
    instance = new CreateProductUsecase(productFactory, dbWrapperMap, helperFunctions)
  }
  return instance
}
