'use strict'
module.exports = function factory () {
  return {
    makeCustomer: (entityObject) => {
      return {
        getCustomerId: () => {
          if (entityObject.id) {
            return entityObject.id
          } else {
            return null
          }
        },
        getCustomerFirstName: () => entityObject.first_name,
        getCustomerLastName: () => entityObject.last_name,
        getCustomerEmail: () => entityObject.email
      }
    }
  }
}
