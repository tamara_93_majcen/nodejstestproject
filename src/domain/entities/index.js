'use strict'
const customerEntity = require ('./customer/customerEntity')
const productEntity = require ('./product/productEntity')
const userEntity = require ('./user/userEntity')
const userEntityName = 'user'
const customerEntityName = 'customer'
const productEntityName = 'product'


class Entities {
  constructor () {
    this.entityMap = new Map()
    this.setEntities()
  }
  setEntities = () => {
    this.entityMap.set(customerEntityName, customerEntity())
    this.entityMap.set(productEntityName, productEntity())
    this.entityMap.set(userEntityName, userEntity())
  }
}
module.exports = new Entities()