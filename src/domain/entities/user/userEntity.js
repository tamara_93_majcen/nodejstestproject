'use strict'
module.exports = function factory () {
  return {
    makeUser: (entityObject) => {
      return {
        getUsername: () => entityObject.username,
        getPassword: () => entityObject.password,
        setHashedPassword: (hashedPassword) => { entityObject.password = hashedPassword }
      }
    }
  }
}
