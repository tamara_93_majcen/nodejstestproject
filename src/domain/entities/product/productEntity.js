'use strict'
module.exports = function factory () {
  return {
    makeProduct: (entityObject) => {
      return {
        getProductId: () => {
          if (entityObject.id) {
            return entityObject.id
          } else {
            return null
          }
        },
        getProductName: () => entityObject.name,
        getProductCustomerId: () => entityObject.customer_id,
        getProductQuantity: () => entityObject.quantity
      }
    }
  }
}
