'use strict'
let instance
class ValidationWorker {
  constructor(validatorConcrete){
    if(!validatorConcrete){
      throw new Error('Validator is undefined, validation worker can not be created')
    }
    this.validatorConcrete = validatorConcrete
  }
  validate = (objectToValidate, schema) => {
    let res = this.validatorConcrete.validate(objectToValidate, schema)
    if (res.length && res.length > 0) {
      return { validationStatus: false, validationError: res.map((errorMsg, index) => errorMsg.message) }
    } else {
      return { validationStatus: true }
    }
  }
}

module.exports = (validatorConcrete) => {
  if(!instance){
    return new ValidationWorker(validatorConcrete)
  }
  return instance
}
