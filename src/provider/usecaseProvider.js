'use strict'
let instance
//USER USECASES
const RegisterUsecase = require('../domain/usecases/entitiesUsecases/userUsecases/registerUsecase')
const LoginUsecase = require('../domain/usecases/entitiesUsecases/userUsecases/loginUsecase')

//CUSTOMER USECASES
const CreateCustomerUsecase = require('../domain/usecases/entitiesUsecases/customerUsecases/createCustomerUsecase')
const ReadCustomerUsecase = require('../domain/usecases/entitiesUsecases/customerUsecases/readCustomerUsecase')
const ReadAllCustomersUsecase = require('../domain/usecases/entitiesUsecases/customerUsecases/readAllCustomersUsecase')
const ReadCustomersProductsUsecase = require('../domain/usecases/entitiesUsecases/customerUsecases/readCustomersProductsUsecase')
const UpdateCustomerUsecase = require('../domain/usecases/entitiesUsecases/customerUsecases/updateCustomerUsecase')
const DeleteCustomerUsecase = require('../domain/usecases/entitiesUsecases/customerUsecases/deleteCustomerUsecase')

// PRODUCT USECASES
const CreateProductUsecase = require('../domain/usecases/entitiesUsecases/productUsecases/createProductUsecase')
const ReadProductUsecase = require('../domain/usecases/entitiesUsecases/productUsecases/readProductUsecase')
const ReadAllProductsUsecase = require('../domain/usecases/entitiesUsecases/productUsecases/readAllProductsUsecase')
const UpdateProductUsecase = require('../domain/usecases/entitiesUsecases/productUsecases/updateProductUsecase')
const DeleteProductUsecase = require('../domain/usecases/entitiesUsecases/productUsecases/deleteProductUsecase')

const customerEntityName = 'customer'
const productEntityName = 'product'
const userEntityName = 'user'
class UsecaseProvider {
  constructor (entityMap, wrapperMap, entityHelperFunctions) {
    this.entityMap = entityMap
    this.wrapperMap = wrapperMap
    this.entityHelperFunctions = entityHelperFunctions
    this.usecaseMap = new Map()
    this.instantiateUsecases()
  }

  instantiateUsecases = () => {

  // USECASES FOR CUSTOMERS
  this.usecaseMap.set('CreateCustomerUsecase', CreateCustomerUsecase(this.entityMap.get(customerEntityName), this.wrapperMap, this.entityHelperFunctions))
  this.usecaseMap.set('ReadCustomerUsecase', ReadCustomerUsecase(this.entityMap.get(customerEntityName), this.wrapperMap, this.entityHelperFunctions))
  this.usecaseMap.set('ReadAllCustomersUsecase', ReadAllCustomersUsecase(this.entityMap.get(customerEntityName), this.wrapperMap, this.entityHelperFunctions))  
  this.usecaseMap.set('ReadCustomersProductsUsecase', ReadCustomersProductsUsecase(this.entityMap.get(customerEntityName), this.wrapperMap, this.entityHelperFunctions))  
  this.usecaseMap.set('UpdateCustomerUsecase', UpdateCustomerUsecase(this.entityMap.get(customerEntityName), this.wrapperMap, this.entityHelperFunctions))
  this.usecaseMap.set('DeleteCustomerUsecase', DeleteCustomerUsecase(this.entityMap.get(customerEntityName), this.wrapperMap, this.entityHelperFunctions))

  // USECASES FOR PRODUCTS
  this.usecaseMap.set('CreateProductUsecase', CreateProductUsecase(this.entityMap.get(productEntityName), this.wrapperMap, this.entityHelperFunctions))
  this.usecaseMap.set('ReadProductUsecase', ReadProductUsecase(this.entityMap.get(productEntityName), this.wrapperMap, this.entityHelperFunctions))
  this.usecaseMap.set('ReadAllProductsUsecase', ReadAllProductsUsecase(this.entityMap.get(productEntityName), this.wrapperMap, this.entityHelperFunctions))
  this.usecaseMap.set('UpdateProductUsecase', UpdateProductUsecase(this.entityMap.get(productEntityName), this.wrapperMap, this.entityHelperFunctions))
  this.usecaseMap.set('DeleteProductUsecase', DeleteProductUsecase(this.entityMap.get(productEntityName), this.wrapperMap, this.entityHelperFunctions))

  // INSTANTIATING USECASES FOR USERS
  this.usecaseMap.set('RegisterUsecase', RegisterUsecase(this.entityMap.get(userEntityName), this.wrapperMap, this.entityHelperFunctions))
  this.usecaseMap.set('LoginUsecase', LoginUsecase(this.entityMap.get(userEntityName), this.wrapperMap, this.entityHelperFunctions))
  }
}
module.exports = (entityMap, wrapperMap, entityHelperFunctions) => {
  if(!instance){
    instance = new UsecaseProvider(entityMap, wrapperMap, entityHelperFunctions)
  }
  return instance
}
