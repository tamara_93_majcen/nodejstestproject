'use strict'
let instance
const RouterManager = require('../managers/routingManager/routing')
const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const cors = require('cors')
const md5 = require('md5')
const moment = require('moment')
const tokenSalt = 'ch28ar15Mw58HHo911S'
const validationSchemas = require('./../managers/validationManager/schemaIndex')
const customErrors = require('./../managers/errorManager/errorManager')
const InvalidAuthenticationDataName = 'InvalidAuthenticationData'
const EntityNotFoundName = 'EntityNotFound'
const EntityDataNotUniqueName = 'EntityDataNotUnique'
class Server {
    constructor (config, usecaseMap, validate) {
    if (!config.serverConfig) {
      throw new Error('Http server configuration is missing!')
    }
    this.serverConfig = config.serverConfig
    this.app = express()
    this.router = express.Router()
    this.usecaseMap = usecaseMap
    this.server = null
    this.middlewares = null
    this.validate = validate
  } 
 
  initServer = () => {
    
    this.app.use(function (req, res, next) {
      res.setHeader('Access-Control-Allow-Origin', '*')
      res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Content-Length, token, username')
      res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS, PUT')
      next()
    })
    
    this.app.use(bodyParser.json())
    this.app.use(bodyParser.urlencoded({ extended: false }))
    this.app.use(cookieParser())

    this.app.post('/register', (req, res, next) => {
      let result = this.validate(req.body, validationSchemas.userSchema)
      if (result.validationStatus) {
        next()
      } else {
        res.status(400).json('Validation failed.' + result.validationError)
      }
    },  async (req, res) => { 
      try{
        let usecase = this.usecaseMap.get('RegisterUsecase')
        await usecase.execute(req.body)
        res.status(200).json('Registration successful.')
      } catch (e) {
        if(e.name === EntityDataNotUniqueName){
          res.status(e.status).json(e.message)
        } else {
          res.status(500).json(e.message)
        }
      }
    })
    
    this.app.post('/login', (req, res, next) => {
      let result = this.validate(req.body, validationSchemas.userSchema)
      if (result.validationStatus) {
        next()
      } else {
        res.status(400).json('Validation failed.' + result.validationError)
      }
    }, async (req, res) => { 
      try{
        let usecase = this.usecaseMap.get('LoginUsecase')
        let userExists =  await usecase.execute(req.body)
        if(userExists){
          let token = md5(req.body.username + moment().format('YYYY/MM/DD') + tokenSalt)
          res.status(200).json({token: token, username: userExists.username})
        } else {
          throw new customErrors.EntityNotFound ('This user doesn\'t exist.')
        }
      } catch (e) {
        if(e.name === EntityNotFoundName || e.name === InvalidAuthenticationDataName){
          res.status(e.status).json(e.message)
        } else {
          res.status(500).json(e.message)
        }
      }
    })

    this.app.use(function (req, res, next) {
      let token = md5(req.headers.username + moment().format('YYYY/MM/DD') + tokenSalt)
      if (req.headers.token === token) {
        next()
      } else {
        res.status(401).json('You are not authorized')
      }
    })

    this.routerManager = RouterManager(this.router, this.usecaseMap, this.validate, validationSchemas)
    this.app.use('/', this.routerManager.router)

    // catch 404 and forward to error handler
    this.app.use(function (req, res, next) {
      let err = new Error('Not Found')
      err.status = 404
      next(err)
    })
    
    // error handler
    this.app.use(function (err, req, res, next) {
      // set locals, only providing error in development
      res.locals.message = err.message
      res.locals.error = req.app.get('env') === 'development' ? err : {}
    
      // render the error page
      res.status(err.status || 500)
      res.json(err)
    })
    
    this.app.listen(this.serverConfig.port, () => {
      console.log(`APP listening at http://${this.serverConfig.webroot}:${this.serverConfig.port}`)
    })
  }
}
module.exports = (config, routes, validate) => {
  if(!instance){
    instance = new Server(config, routes, validate)
  }
  return instance
}
