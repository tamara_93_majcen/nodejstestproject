'use strict'
module.exports = (sequelize, DataTypes) => {
  let customer = sequelize.define('customer', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    first_name: {
      type: DataTypes.STRING(25),
      allowNull: false
    },
    last_name: {
      type: DataTypes.STRING(25),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(100),
      allowNull: false,
      unique: true
    }
  }, {
    tableName: 'customers',
    timestamps: false
  })
  customer.associate = (models) => {
    models.customer.hasMany(models.product, {
      foreignKey: 'customer_id',
      sourceKey: 'id',
      onDelete: 'cascade'
    })
  }
  return customer
}
