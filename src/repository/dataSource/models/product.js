'use strict'
module.exports = (sequelize, DataTypes) => {
  let product = sequelize.define('product', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false,
      unique: true
    },
    quantity: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    customer_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }
  }, {
    tableName: 'products',
    timestamps: false
  })

  product.associate = (models) => {
    models.product.belongsTo(models.customer, {
      foreignKey: 'customer_id',
      sourceKey: 'id'
    })
  }
  return product
}
