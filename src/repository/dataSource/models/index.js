'use strict'
const fs = require('fs')
const path = require('path')
const basename = path.basename(__filename)
let instance

class Models {
  constructor (sequelize) {
    if (!sequelize) {
      throw new Error('ORM is undefined, models can not be created')
    }
    this.db = {}
    this.sequelize = sequelize
  }
  associateModels () {
    fs
      .readdirSync(__dirname)
      .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
      })
      .forEach(file => {
        let model = this.sequelize['import'](path.join(__dirname, file))
        this.db[model.name] = model
      })

    Object.keys(this.db).forEach(modelName => {
      if (this.db[modelName].associate) {
        this.db[modelName].associate(this.db)
      }
    })
  }
}

module.exports = (sequelize) => {
  if (!instance) {
    instance = new Models(sequelize)
  }
  return instance
}
