'use strict'
let instance
class DbOperations {
  constructor (models) {
    this.models = models
  }

  async create ({ entityName, object }) {
    let model = this.models[entityName]
    let created = await model.create(object)
    return created
  }

  async readOne ({ entityName, readParameterName, readParameterValue }) {
    // read parameter name ce biti ili email ili id ili username
    let model = this.models[entityName]
    let entity = await model.findOne({
      where: {
        [readParameterName]: readParameterValue
      }
    })
    return entity
  }

  async read ({ entityName, readParameterName = null, readParameterValue = null }) {
    let model = this.models[entityName]
    let entities
    if (readParameterValue && readParameterName) {
      entities = await model.findAll({
        where: {
          [readParameterName]: readParameterValue
        }
      })
    } else {
      entities = await model.findAll()
    }
    return entities
  }

  async update ({ entityName, updatedObject, id }) {
    let model = this.models[entityName]
    let updated = await model.update(updatedObject, {
      where: {
        id: id
      }
    })
    return updated[0]
  }
  async delete ({ entityName, deleteId }) {
    let model = this.models[entityName]
    let deleted = await model.destroy({
      where: {
        id: deleteId
      }
    })
    return deleted
  }
}
module.exports = (models) => {
  if (!instance) {
    instance = new DbOperations(models)
  }
  return instance
}
