'use strict'
let instance
class EntityHelperFunctions {
  constructor (hashConcrete, passwordSalt) {
    if (!hashConcrete || !passwordSalt) {
      throw new Error('Hash function or password salt are undefined, entity helper functions can not be created')
    }
    this.hashConcrete = hashConcrete
    this.passwordSalt = passwordSalt
  }
  hashPassword (password) {
    let hashedPassword = this.hashConcrete(password + this.passwordSalt)
    return hashedPassword
  }
  async entityExists (entityName, dbWrapperMap, readParameterName, readParameterValue) {
    let dbWrapper = dbWrapperMap.get(entityName)
    let entity = await dbWrapper.readOne(readParameterName, readParameterValue)
    return entity
  }
}

module.exports = (hashConcrete, passwordSalt) => {
  if (!instance) {
    instance = new EntityHelperFunctions(hashConcrete, passwordSalt)
  }
  return instance
}
