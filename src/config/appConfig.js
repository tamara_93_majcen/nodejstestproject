'use strict'
const env = require('node-env-file')

// GENERAL
let envirnoment = process.env.NODE_ENV || 'development'
if (envirnoment === 'development') {
  try {
    env('.env')
  } catch (e) {
    console.log('.env file missing.')
  }
}

// SERVER
let webroot = process.env.WEBROOT || 'localhost'
let serverPort = process.env.SERVER_PORT || 8003

//DB
let username = process.env.DB_USERNAME || 'root'
let password = process.env.DB_PASSWORD || '1'
let databaseName = process.env.DB_NAME || 'nodejs_test'
let host = process.env.DB_HOST || 'localhost'
let port = process.env.DB_PORT || 3306
let dialect = process.env.DB_DIALECT || 'mysql'

module.exports = {
  general: {
    env: envirnoment
  },
  serverConfig: {
    webroot: webroot,
    port: serverPort
  },
  databaseConfig: {
    username: username,
    password: password,
    databaseName: databaseName,
    host: host,
    port: port,
    dialect: dialect
  }
}
