# Node.js test project

Test project for Codeus with basic CRUD operations for Customer and Product entities


## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

## Project information

Core of this application are entities (user, customer and product). All functions for working with these entities are called usecases and they are called from the routing file.
When starting app, first register a user, then login with the registered user (because you need token and its username for every other route)

## Prerequisites

- Node.js 13.1.0

- Mysql 5.7.26


## Common setup

Clone the repo and install the dependencies inside the cloned repository directory by running:

```
npm install
```


Create **.env** file using **.env-example**

Set Sequelize configuration in **/src/config/config.json**

Create database and then run following command from the **/src** folder in order to run migrations:

```
npx sequelize-cli db:migrate
```

To run Express server, run following command:

```
npm start
```

## References

You can find Postman collection on following URL:

https://www.getpostman.com/collections/ae3f1ebd8490689992d0 

Before importing following json as Postman environment, set **"value": "http://host:port/"**
```
{
	"id": "a49a57a5-64a5-4b6f-bc94-94b4aa06b0d0",
	"name": "NodejsTestEnvironment",
	"values": [
		{
			"key": "host",
			"value": "",
			"enabled": true
		},
		{
			"key": "token",
			"value": "",
			"enabled": true
		},
		{
			"key": "username",
			"value": "",
			"enabled": true
		}
	],
	"_postman_variable_scope": "environment",
	"_postman_exported_at": "2019-11-21T07:38:13.106Z",
	"_postman_exported_using": "Postman/7.12.0"
}
```

Project documentation can be found on following URL:

https://docs.google.com/document/d/1lOb3mnAtYhH_Ro-PGsl99l2WEZqfMYR0UUdm5U2qV2U/edit?usp=sharing

