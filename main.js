'use strict'
let instance
const md5 = require('md5')
const passwordSalt = 'heLlO2U2sTR@nGer'
const Validator = require('fastest-validator')
const validatorInstance = new Validator()
// ORM
const Sequelize = require('sequelize')
// repo
const ValidationWorker = require('./src/workers/validationWorker')
const ModelsConcrete = require('./src/repository/dataSource/models/index')
const DbOperationsConcrete = require('./src/repository/dataSource/dbOperations')
// domain
const Server = require('./src/server/server')
const Entities = require('./src/domain/entities/index')
// managers
const ValidationManager = require('./src/managers/validationManager/validationManager')
// wrappers
const EntitiesDbWrappers = require('./src/wrappers/index')
// helpers
const EntityHelperFunctions = require('./src/helpers/entityHelperFunctions')
// providers
const UsecaseProvider = require('./src/provider/usecaseProvider')

class Main {
  constructor (config) {
    this.config = config
    this.entityHelperFunctions = EntityHelperFunctions(md5, passwordSalt)
  }
  start = () => {
    this.entities = Entities
    this.validationWorker = ValidationWorker(validatorInstance)
    this.validationManagerInst = ValidationManager(this.validationWorker)

    this.sequelize = new Sequelize(this.config.databaseConfig.databaseName, this.config.databaseConfig.username, this.config.databaseConfig.password, {
      host: this.config.databaseConfig.host,
      dialect: this.config.databaseConfig.dialect
    })

    //TODO: HANDLE RECONNECTION
    this.sequelize.authenticate()
    .then(() => {
      console.log('Connection has been established successfully.')
      // CREATING CONCRETE DB OPERATIONS
      this.modelsConcrete = ModelsConcrete(this.sequelize)
      this.modelsConcrete.associateModels()
      this.dbOperationsConcrete = DbOperationsConcrete(this.modelsConcrete.db)

      // CREATING DB WRAPPERS
      this.entitiesDbWrappers = EntitiesDbWrappers(this.dbOperationsConcrete, this.entities.entityMap, this.entityHelperFunctions)

      // CREATING USECASES
      this.usecaseProvider = UsecaseProvider(this.entities.entityMap, this.entitiesDbWrappers.wrapperMap, this.entityHelperFunctions)

      // CREATING SERVER
      this.server = Server(this.config, this.usecaseProvider.usecaseMap, this.validationManagerInst.validate)
      this.server.initServer()
    })
    .catch(err => {
      console.error('Unable to connect to the database:', err)
    })
    }
}
module.exports = (config) => {
  if(!instance){
    instance = new Main(config)
  }
  return instance
}