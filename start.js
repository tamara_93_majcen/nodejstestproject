'use strict'
const config = require('./src/config/appConfig')
const Main = require('./main')

let main = Main(config)
try {
  main.start()
} catch (e) {
  console.log(e.message)
}
